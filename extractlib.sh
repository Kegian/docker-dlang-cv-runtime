#!/bin/sh
VERSION=$(cat VERSION)

docker run --rm \
    -v $(pwd)/copy.sh:/tmp/copy.sh:Z \
    -v $(pwd)/libs/pkgconfig:/tmp/pkgconfig \
    -v $(pwd)/libs/so:/tmp/so \
    -v $(pwd)/libs/python:/tmp/python \
    registry.gitlab.com/kegian/docker-dlang-cv-dev:$VERSION /tmp/copy.sh

