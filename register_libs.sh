#!/bin/bash

PTH=/usr/local/lib
PREFIX=libopencv

for SRC in $(ls ${PTH}/${PREFIX}_*.so.${OPENCV_LIB_VER_FULL}); do
    TRG_MID=$(echo ${SRC} | sed "s/.${OPENCV_LIB_VER_FULL}/.${OPENCV_LIB_VER_MID}/")
    ln -sf ${SRC} ${TRG_MID}
    TRG=$(echo ${TRG_MID} | sed "s/.${OPENCV_LIB_VER_MID}//")
    ln -sf ${TRG_MID} ${TRG}
done

ln -sf /usr/local/lib/libdlib.so.${DLIB_LIB_VER_FULL} /usr/local/lib/libdlib.so

ldconfig
