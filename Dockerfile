FROM registry.gitlab.com/o.lelenkov/docker-dlang-runtime:2.81.2
MAINTAINER Oleg Lelenkov "o.lelenkov@gmail.com"

ENV OPENCV_LIB_VER_FULL=3.4.1
ENV OPENCV_LIB_VER_MID=3.4
ENV DLIB_LIB_VER_FULL=19.7.0

ADD ./libs/pkgconfig /usr/local/lib/pkgconfig
ADD ./libs/so /usr/local/lib
ADD ./libs/python /usr/local/lib/python2.7/dist-packages

COPY ./register_libs.sh /tmp/register_libs.sh
RUN chmod +x /tmp/register_libs.sh \ 
    && sh /tmp/register_libs.sh \
    && rm -rf /tmp/register_libs.sh

RUN install_packages libblas3 libopenblas-base liblapack3 \
        libavcodec57 libavformat57 libavutil55 libswscale4 libgomp1 libtbb2 \
        python python-pip python-numpy \
        && ldconfig

RUN pip install requests
