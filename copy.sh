#!/bin/bash

DLIB_VER=19.7.0
OPENCV_VER=3.4.1

rm -rf /tmp/pkgconfig/*
rm -rf /tmp/so/*
rm -rf /tmp/python/*

cp /usr/local/lib/pkgconfig/dlib-1.pc /tmp/pkgconfig
cp /usr/local/lib/pkgconfig/opencv.pc /tmp/pkgconfig

cp /usr/local/lib/libdlib.so.${DLIB_VER} /tmp/so
cp /usr/local/lib/libopencv_*.so.${OPENCV_VER} /tmp/so
cp /usr/local/lib/python2.7/dist-packages/*.so /tmp/python
