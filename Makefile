all: build

extract:
	@./extractlib.sh

build: extract
	@docker build --rm --tag=registry.gitlab.com/o.lelenkov/docker-dlang-cv-runtime:latest .

release: build
	@docker build --rm --tag=registry.gitlab.com/o.lelenkov/docker-dlang-cv-runtime:$(shell cat VERSION) .

publish: release
	@docker push registry.gitlab.com/o.lelenkov/docker-dlang-cv-runtime
